(ns clj-db-lib.sql
  (:require
   [clj-data-lib.data :refer [data-maps->vector or+ vector->data-maps]]
   [clj-data-lib.string :refer [ci=]]
   [clj-data-lib.time :as time]
   [clj-data-lib.translate :refer [str->char]]
   [clj-db-lib.provider :as p]
   [clj-db-lib.utils.data :as dd]
   [clj-db-lib.utils.strict
    :refer [data-set->strict map->strict-opts table->str]]
   [clj-refile.formats.gzip :as gzip]
   [clojure.data.csv :as csv]
   [clojure.java.io :as io]
   [clojure.string :as string]
   [honeysql-plus.core :as hh+]
   [next.jdbc :as jdbc]
   [next.jdbc.prepare :as jdbc-p]
   [next.jdbc.result-set :as jdbc-rs]
   [next.jdbc.sql :as jdbc-sql]
   [taoensso.timbre :as log]))

(def batch-size-db-writes 10000)

(defn- throw-sql-error [error function & [query-or-input]]
  (let [err (str "SQL exception: " error ", executing function: " function
                 (when query-or-input (str "\nQuery/Input executed when error:\n" query-or-input)))]
    (log/fatal err)
    (throw (Exception. err))))

(defn- read-query-opts
  ([]
   {:builder-fn jdbc-rs/as-unqualified-lower-maps})
  ([{:keys [provider] :as execute-opts}]
   (merge {:builder-fn (if (dd/as-arrays? execute-opts) jdbc-rs/as-arrays jdbc-rs/as-unqualified-lower-maps)}
          (p/provider-read-query-opts provider))))

(defn- datasource-supports-transactions? [conn]
  (let [ds (if (and (map? conn) (:datasource conn))
             (:datasource conn)
             conn)]
    (when-not (instance? javax.sql.DataSource ds)
      (throw (IllegalArgumentException. (str ds " is not a datasource"))))
    (with-open [conn (.getConnection ds)]
      (-> conn
          .getMetaData
          .supportsTransactions))))

(defmacro with-transaction [[sym transactable opts] & body]
  `(let [conn# ~transactable]
     (if (datasource-supports-transactions? conn#)
       (jdbc/with-transaction [~sym conn# ~opts]
         ~@body)
       (let [~sym conn#]
         ~@body))))

; TODO fix to support synapse ddl statements
(defmulti execute-one! (fn [_ query] (class query)))
(defmethod execute-one! String [db-conn query]
  (execute-one! db-conn (vector query)))
(defmethod execute-one! :default [db-conn query]
  (try
    (with-transaction
      [conn (:datasource db-conn)]
      (log/debug "Executing execute-one!:\n" query)
      (jdbc/execute-one! conn query (read-query-opts)))
    (catch Exception e
      (throw-sql-error e "execute-one!" query))))

(defn jdbc-execute!
  ([db-conn query]
   (jdbc-execute! db-conn query {}))
  ([db-conn query {:keys [provider max-retries]
                   :or {max-retries (p/default-max-retries-for-provider provider)}
                   :as opts}]
   (try
     (log/debug "Executing execute!:\n" query)
     (if (and (p/no-transaction-provider? provider) (dd/sql-is-ddl? query))
       (do
         (log/debug "Executing outside transaction for provider: " provider)
         (jdbc/execute! (:datasource db-conn) query (read-query-opts)))
       (with-transaction
         [conn (:datasource db-conn)]
         (jdbc/execute! conn query (read-query-opts))))
     (catch Exception e
       (if (and max-retries (pos? max-retries))
         (do
           (log/warn "Execution during query:" query e)
           (log/warn "Retrying" max-retries "more times")
           (jdbc-execute! db-conn query
                          (assoc opts :max-retries (dec max-retries))))
         (throw-sql-error e "jdbc-execute!" query))))))

(defn- filter-tables [data table]
  (let [[schema tbl] (table->str table)]
    (some
     #(when (and (or (nil? schema) (ci= (:table_schem %) schema))
                 (ci= (:table_name %) tbl))
        %)
     data)))

(defn get-metadata-columns
  "Return metadata columns for a single table."
  [db-conn table]
  (try
    (with-transaction
      [conn (:datasource db-conn)]
      (log/debug "Executing get-metadata-columns for table: " table)
      (when-let [{:keys [table_schem table_name]}
                 (-> (.getMetaData conn)
                     (.getTables nil nil nil (into-array ["TABLE"]))
                     (jdbc-rs/datafiable-result-set {} (read-query-opts))
                     (filter-tables table)
                     not-empty)]
        (log/debug "Retrieving data columns for schema: " table_schem " and table: " table_name)
        (-> (.getMetaData conn)
            (.getColumns nil table_schem table_name nil)
            (jdbc-rs/datafiable-result-set {} (read-query-opts)))))
    (catch Exception e
      (throw-sql-error e "get-metadata-columns"))))

(defn ->strict [data db-conn {:keys [table provider] :as input}]
  (if (p/strict-type-provider? provider)
    (let [strict-opts (map->strict-opts input)
          table-ddl (get-metadata-columns db-conn table)]
      (data-set->strict data table-ddl strict-opts))
    data))

(defn ->quoted [v {:keys [quote-when-fn] :as opts}]
  (if quote-when-fn (dd/quote-identifiers v opts) v))

(defn insert! [db-conn table row & [{:keys [provider] :as opts}]]
  (try
    (log/debug "Insert into " table ", row: " row)
    (let [data-row (-> row
                       (->strict db-conn {:table table :provider provider})
                       (->quoted opts))]
      (with-transaction
        [conn (:datasource db-conn)]
        (let [log-conn (next.jdbc/with-logging conn #(log/debug %1 %2))]
          (jdbc-sql/insert! log-conn (->quoted table opts) data-row (p/provider-insert-opts provider)))))
    (catch Exception e
      (throw-sql-error e "insert!"))))

(defn insert-multi!
  ([db-conn table rows opts]
   (let [ks (->> rows first keys (into []))]
     (insert-multi! db-conn table ks (data-maps->vector ks rows) opts)))
  ([db-conn table columns rows {:keys [provider batch-size] :as opts}]
   (log/debug "Inserting into table: " table ", columns:" columns)
   (try
     (let [ds-rows (->strict rows db-conn {:columns columns :table table :provider provider})
           ps-stmt (when (p/batch-provider? provider)
                     (hh+/insert-prepared-statement table columns (p/hh-opts opts)))
           bs (or batch-size (p/provider-batch-size provider) batch-size-db-writes)]
       (if (p/batch-provider? provider)
         (with-transaction
           [conn (:datasource db-conn)]
           (log/debug "Batch provider statement:\n" ps-stmt)
           (with-open
            [ps (jdbc/prepare conn ps-stmt)]
             (jdbc-p/execute-batch! ps ds-rows {:batch-size bs})))
         (with-transaction
           [conn (:datasource db-conn)]
           (doseq [batch (partition-all bs ds-rows)]
             (log/debug "batch" (string/join ", " batch))
             (let [log-conn (next.jdbc/with-logging conn #(log/debug "insert-multi!" %1 %2))]
               (jdbc-sql/insert-multi!
                log-conn (->quoted table opts) (->quoted (map keyword columns) opts)
                batch))))))
     (catch Exception e
       (throw-sql-error e "insert-multi!" (str "table: " table " and columns: " columns))))))

(defn update!
  [db-conn table row where-clause & [{:keys [provider] :as opts}]]
  (try
    (log/debug "Updating table: " table ", updated row: " row ", where: " where-clause)
    (with-transaction
      [conn (:datasource db-conn)]
      (let [data-row (-> row
                         (->strict db-conn {:table table :provider provider})
                         (->quoted opts))]
        (jdbc-sql/update! conn (->quoted table opts) data-row (->quoted where-clause opts))))
    (catch Exception e
      (throw-sql-error e "update!" row))))

(defn update-or-insert!
  "Updates row or inserts new in the specified table"
  ([db-conn table row sql-where & [{:keys [provider] :as opts}]]
   (try
     (log/debug "Update/insert into table: " table ", row: " row ", where: " sql-where)
     (with-transaction
       [conn (:datasource db-conn)]
       (let [tbl (->quoted table opts)
             data-row (-> row
                          (->strict db-conn {:table table :provider provider})
                          (->quoted opts))
             result (jdbc-sql/update! conn tbl data-row (->quoted sql-where opts))]
         (if (zero? (::jdbc/update-count result))
           (let [log-conn (next.jdbc/with-logging conn #(log/debug %1 %2))]
             (jdbc-sql/insert! log-conn tbl data-row (p/provider-insert-opts provider)))
           result)))
     (catch Exception e
       (log/error e)
       (throw-sql-error e "update-or-insert!" row)))))

(declare execute!)
(defn execute-many!
  "Takes input connection and a vector of queries to be executed. Only last query result is returned."
  ([db-conn query-vector]
   (execute-many! db-conn query-vector {}))
  ([db-conn query-vector opts]
   (log/debug "execute-many!" query-vector)
   (let [*res* (atom nil)]
     (doseq [exec-query query-vector]
       (reset! *res* (execute! db-conn exec-query opts)))
     @*res*)))

(defn- query-string->queries
  "Split query string on semi-colon into vector of strings (omitting blanks/newlines/etc)."
  [v]
  (->> (string/split v #";")
       (map string/trim)
       (filter #(when (pos? (count %)) %))
       (map vector) vec))

(defn single-query-provider-queries
  "For providers that do not support multiple queries in a single API call split query string into multiple queries.

  Because execute occurs in a with-transaction block, any changes within the block should be rolled back if
  transaction fails.
  "
  [provider query]
  (when (and (p/single-api-query-provider? provider) (= (count query) 1))
    (let [queries (-> query first query-string->queries)]
      (when (> (count queries) 1)
        queries))))

(defn execute!
  "Takes input connection and query to be executed."
  [db-conn query {:keys [provider] :as opts}]
  (log/debug "execute!" query)
  (if-let [split-queries (single-query-provider-queries provider query)]
    (execute-many! db-conn split-queries opts)
    (jdbc-execute! db-conn query opts)))

(defn- data->limit-records [{:keys [limit-rows] :or {limit-rows -1}} data]
  (if (pos? limit-rows)
    (take limit-rows data)
    data))

(defn execute!->file
  [db-conn query output-path
   {:keys [gzip include-header separator reducer-fn-out columns] :as execute-opts}]
  (try
    (log/debug "Executing execute!->file:\n" query)
    (log/info "Creating parent directories for: " output-path)
    (io/make-parents output-path)
    (let [gzip? (or+ gzip true)
          include-header? (or+ include-header true)
          output-filepath (if gzip? (str output-path ".gz") output-path)
          separator-char (str->char separator)
          output-columns (->> (or reducer-fn-out columns)
                              (map name) vec)]
      (with-open [writer (gzip/get-writer output-filepath gzip?)]
        (when include-header?
          (csv/write-csv writer [output-columns] :separator separator-char))
        (with-transaction
          [conn (:datasource db-conn)]
          (->> (read-query-opts execute-opts)
               (jdbc/plan conn query)
               (reduce (dd/data->reducer-fn-file writer separator-char execute-opts) []))))
      {:last_time_updated (time/instant-as-str)
       :output_columns output-columns
       :files-extracted [output-filepath]})
    (catch Exception e
      (throw-sql-error e "execute!->file" query))))

(defn execute!-with-reducer
  [db-conn query {:keys [columns reducer-fn-out] :as execute-opts}]
  (try
    (log/debug "Executing execute!-with-reducer:\n" query)
    (with-transaction
      [conn (:datasource db-conn)]
      (->> (jdbc/plan conn query (read-query-opts execute-opts))
           (reduce (dd/data->reducer-fn execute-opts) [])
           (data->limit-records execute-opts)
           (vector->data-maps (or reducer-fn-out columns))))
    (catch Exception e
      (throw-sql-error e "execute!-with-reducer" query))))

(defn jdbc-plan
  [db-conn query {:keys [reducer-fn] :as execute-opts}]
  (try
    (log/debug "Executing jdbc-plan:\n" query)
    (with-transaction
      [conn (:datasource db-conn)]
      (reduce
       reducer-fn
       []
       (jdbc/plan conn query (read-query-opts execute-opts))))
    (catch Exception e
      (throw-sql-error e "jdbc-plan" query))))

(defn csv->table
  "Load CSV into DB table."
  [db-conn input-file table
   {:keys [separator columns header-line gzip transform-fn]
    :or   {gzip true} :as execute-opts}]
  (log/debug "Executing CSV to table from file: " input-file ", to table: " table ", using opts: " execute-opts)
  (try
    (with-open [in-file (gzip/get-reader input-file gzip)]
      (let [data (csv/read-csv in-file :separator (str->char separator))
            column-keys (or columns (first data))
            values (if header-line (rest data) data)]
        (log/debug "File keys: " column-keys)
        (doseq [batch (partition-all batch-size-db-writes values)]
          (insert-multi!
           db-conn table column-keys
           (if transform-fn (transform-fn batch) batch)
           execute-opts))))
    (catch Exception e
      (throw-sql-error e "csv->table" (str "input file: " input-file " and table: " table)))))
