(ns clj-db-lib.spec.connections
  (:require [clojure.spec.alpha :as s]))

(def supported-providers #{:h2 :mssql :openedge :oracle :postgres :snowflake :synapse :databricks})

(s/def ::provider supported-providers)
(s/def ::host string?)
(s/def ::port (s/int-in 1024 65535))
(s/def ::dbname string?)
(s/def ::user string?)
(s/def ::pswd string?)
(s/def ::schema string?)
(s/def ::ds-props map?)
(s/def ::session-queries (s/* string?))
(s/def ::pool-name string?)
(s/def ::idle-timeout (s/and integer? pos?))
(s/def ::maximum-pool-size (s/and integer? pos?))
(s/def ::minimum-idle (s/and integer? pos?))
(s/def ::connection-timeout (s/and integer? pos?))

; PROVIDER SPECIFIC
; H2
(s/def ::h2_path string?)
; SQL Server
(s/def ::auth_scheme string?)
; ORACLE
(s/def ::sid string?)
; SNOWFLAKE
(s/def ::warehouse string?)
(s/def ::role string?)

(s/def ::db-connection
  (s/keys :req [::provider]
          :opt [::host ::port ::dbname ::user ::pswd ::schema ::ds-props
                ::h2_path ::auth_scheme ::sid ::warehouse ::role ::session-queries]))
