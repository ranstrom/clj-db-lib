# clj-db-lib

Library for handling some boilerplate DB functions with some opinionated defaults.

## Supported functions

- Connection Pooling \(HikariCP\)
- SQL functions \(using next-jdbc\)

## Supported Providers

| Provider  | Supported |
| :-------- | :-------- |
| h2        | Yes       |
| mssql     | Yes       |
| oracle    | Yes       |
| postgres  | Yes       |
| snowflake | Yes       |

## Using

### Opening a connection

```
; Opening a single connection
(with-open [conn (clj-db-lib.connections/db-pooled-spec properties)]
  ...)

; Opening a map with many connections
(def conn-properties
  {:conn-a {...} :conn-b {...}})

(with-open [conn-map (clj-db-lib.connections-map/db-conn-map) [:conn-a :conn-b]]
  ...)
```


### Quiet the logs!

HikariCP can be a bit noisy. To handle this, blacklist namespace `com.zaxxer.hikari.*`.

```
; Using Timbre
(taoensso.timbre/merge-config! {:ns-blacklist ["com.zaxxer.hikari.*"]})
```

## Disclaimer

**THIS CODE AND INFORMATION IS PROVIDED "AS IS" WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A PARTICULAR PURPOSE.**